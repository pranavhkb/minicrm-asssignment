<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Employee;

class EmployeeInfo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'employee:abc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'getting employee';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $employee = Employee::get();

        $this->info($employee);
    }
}
