<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Company;

class CompanyInfo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'company:info';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This is the command used for retriving comapny information';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $company = Company::get();
        $this->info($company[0]);
    }
}
