<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Company;
use Illuminate\Support\Facades\Storage;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['company'] = Company::orderBy('id','desc')->paginate(5);

        // return view('company.index', $data);
        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return view('company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|email',
            'logo' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048|dimensions:max_width=100,max_height=100',
            'website' => 'required',
        ]);
        
        $input = $request->all();

        if($request->hasFile('logo')){    
            $path = $request->file('logo')->store('public/logos');
            $input['logo'] = Storage::path($path);
        }else{
            unset($input['logo']);
        }

        $company = Company::create($input);

        return response()->json($company);

     
        // return redirect()->route('posts.index')
        //                 ->with('success','Post has been created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['company'] = Company::with(['employees'])
                                    ->whereId($id)
                                    ->orderBy('id','desc')
                                    ->get();

        return response()->json($data);
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'string',
            'email' => 'email',
            'logo' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048|dimensions:max_width=100,max_height=100',
            'website' => 'string',
        ]);
        
        $company = Company::find($id);

        $input = $request->all();

        if($request->hasFile('logo')){        
            $path = $request->file('logo')->store('public/logos');
            $input['logo'] = $path;
        }else{
            unset($input['logo']);
        }
       
        $company->update($input);

        return response()->json($company);

    
        // return redirect()->route('company.index')
        //                 ->with('success','Company updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Company::find($id);
        $company->delete();
    
        return response()->json($company);
        // return redirect()->route('company.index')
        //                 ->with('success','Company has been deleted successfully');
    }
}
